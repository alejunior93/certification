package net.openid.conformance.openbanking_brasil.plans;

public class PlanNames {
	public static final String ACCOUNT_API_NAME = "Functional tests for accounts API - based on Swagger version: 1.0.2 (WIP)";
	public static final String CONSENTS_API_NAME = "F2 T0 Functional tests for consents API - based on Swagger version: 1.0.3";
	public static final String CREDIT_CARDS_API_PLAN_NAME = "Functional tests for credit card API - based on Swagger version: 1.0.2 (WIP))";
	public static final String CREDIT_OPERATIONS_API_PLAN_NAME = "Functional tests for credit operations API - based on Swagger version: 1.0.2 (WIP)";
	public static final String CUSTOMER_PERSONAL_DATA_API_PLAN_NAME = "F2 T0 Functional tests for personal customer data API - based on Swagger version: 1.0.3";
	public static final String CUSTOMER_BUSINESS_DATA_API_PLAN_NAME = "F2 T0 Functional tests for business customer data API - based on Swagger version: 1.0.3";
	public static final String RESOURCES_API_PLAN_NAME = "F2 T0 Functional tests for resources API - based on Swagger version: 1.0.2";
	public static final String CREDIT_OPERATIONS_ADVANCES_API_PLAN_NAME = "Functional tests for unarranged overdraft API - based on Swagger version: 1.0.2 (WIP)";
	public static final String LOANS_API_PLAN_NAME = "Functional tests for loans API - based on Swagger version: 1.0.2 (WIP)";
    public static final String FINANCINGS_API_NAME = "Functional tests for financings API - based on Swagger version: 1.0.2 (WIP)";
	public static final String CREDIT_OPERATIONS_DISCOUNTED_CREDIT_RIGHTS_API_PLAN_NAME = "Functional tests for discounted credit rights API - based on Swagger version: 1.0.2 (WIP)";
}
